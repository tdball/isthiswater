function getParameter(parameterName) {
    let result
    let parameters = location.search
        .substr(1)
        .replace("?", "")
        .split("&")
    if(parameters) {
        for(let parameter of parameters) {
            let data = parameter.split('=')
            let key = data[0]
            let value = data[1]
            if(key === parameterName) {
                result = decodeURIComponent(value)
            }
        }
    } else {
        return null
    }
    return result
}


function checkForWater() {
    let latitude = getParameter('lat')
    let longitude = getParameter('lng')
    let answer = document.getElementById('answer')
    let latitude_input = document.getElementById('latitude')
    let longitude_input = document.getElementById('longitude')
    let reset = document.getElementById('reset')
    reset.style.display = 'none'
    if(latitude || longitude) {
        hideInput()
        isInWater(latitude, longitude)
            .then((inWater) => populateAnswer(inWater))
    }
}
function populateAnswer(data) {
    if (data.invalidData) {
        answer.innerText = 'Well you buggered that up.'
    } else if (data.inWater) {
        answer.innerText = "Yup."
    } else {
        answer.innerText = "Nope."
    }
    document.getElementById('reset').style.display = 'block'
}

function resetLookup() {
    initializeSearchFields('latitude')
    initializeSearchFields('longitude')
    document.getElementById('reset').style.display = 'none'
    document.getElementById('search').style.display = 'block'
}

function initializeSearchFields(id) {
    let searchInput = document.getElementById(id)
    searchInput.value = null
    searchInput.style.display = 'block'
    document.getElementById('answer').innerText = null
}
function hideInput() {
    document.getElementById('latitude').style.display = 'none'
    document.getElementById('longitude').style.display = 'none'
    document.getElementById('search').style.display = 'none'
}


function errorHandler(error) {
    console.log(error)
}


function isInWater(latitude, longitude) {
    document.getElementById('answer').innerText = 'Hmmm...'
    return fetch(`https://isthiswater.info/api/?lat=${latitude}&lng=${longitude}`)
        .then((response) => response.json())
        .then((data) => data)
        .catch((error) => errorHandler)
}

function updateURL () {
    let latitudeInput = document.getElementById('latitude')
    let longitudeInput = document.getElementById('longitude')
    if(latitudeInput.value && longitudeInput.value) {
        isInWater(latitudeInput.value, longitudeInput.value)
            .then((inWater)=>populateAnswer(inWater))
        latitudeInput.style.display = 'none'
        longitudeInput.style.display = 'none'
        document.getElementById('search').style.display = 'none'
    }

}
