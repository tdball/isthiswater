FROM python:3.6

COPY . /api/

WORKDIR /api

RUN pip install -r requirements.txt && \
    useradd -M gunicorn

EXPOSE 3100

ENTRYPOINT gunicorn -c config.py isitwater:api