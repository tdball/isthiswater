import json
from functools import lru_cache
import falcon
from falcon_cors import CORS
import fiona
from shapely.geometry import Point, asShape

from config import SHAPEFILE_PATH


class InWaterResource:
    @staticmethod
    def valid_latlng(latitude, longitude):
        if -90 <= latitude <= 90:
            if -180 <= longitude <= 180:
                return True
            else:
                return False
        else:
            return False

    def on_get(self, request, response):
        latitude = request.get_param('lat')
        longitude = request.get_param('lng')
        if latitude and longitude:
            latitude = float(latitude)
            longitude = float(longitude)
            if self.valid_latlng(latitude, longitude):
                response.status = falcon.HTTP_200
                response.body = json.dumps({"inWater": self.in_water(latitude, longitude)})
            else:
                response.status = falcon.HTTP_404
                response.body = json.dumps({"invalidData": "`lat` or `lng` out of range"})
        else:
            response.status = falcon.HTTP_200
            response.body = json.dumps({"invalidData": "Please submit `lat` and `lng` parameters"})


    @staticmethod
    @lru_cache(maxsize=128)
    def in_water(latitude: float, longitude: float) -> bool:
        """
        Simple function to parse a shapefile from OpenStreet Maps.
        Returns a boolean signifying a location is or is not over
        water.

        LRU Caching provided via functools.lru_cache. Uses memoization
        to cache recently scanned results and provide large performance gains.

        :param latitude: float
        :param longitude: float
        :return: bool
        """
        path = SHAPEFILE_PATH
        with fiona.open(path) as fiona_collection:
            box_detail = 0.0001
            point = Point(longitude, latitude)
            # here we filter to only scan results near the point in question.
            for record in fiona_collection.filter(bbox=(
                    longitude+box_detail, latitude+box_detail,
                    longitude-box_detail, latitude-box_detail)):
                if record['geometry']:
                    shape = asShape(record['geometry'])
                    if shape.contains(point):
                        return True
            return False


cors = CORS(allow_all_origins=True)
api = falcon.API(middleware=[cors.middleware])
api.add_route('/api', InWaterResource())
