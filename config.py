import os
from multiprocessing import cpu_count

# Gunicorn configuration section
bind = "0.0.0.0:3100"
workers = cpu_count() + 1
reload = True
user = "gunicorn"
group = "gunicorn"

# Constants for the API
SHAPEFILE_PATH = os.path.abspath('assets/')